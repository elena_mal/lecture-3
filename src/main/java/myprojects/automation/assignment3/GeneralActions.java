package myprojects.automation.assignment3;

import myprojects.automation.assignment3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private static WebDriver driver;

    private static WebDriverWait wait;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public static void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        driver.navigate().to(Properties.getBaseAdminUrl());
        waitForContentLoad("email");
//        driver.navigate().to("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement email = driver.findElement(By.id("email"));

        email.sendKeys(login);
        WebElement pass = driver.findElement(By.id("passwd"));

        pass.sendKeys(password);
        pass.submit();
        throw new UnsupportedOperationException();
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createCategory(String categoryName) {
        // TODO implement logic for new category creation
        throw new UnsupportedOperationException();

    }

    /**
     * Waits until page loader disappears from the page
     */
    public static void waitForContentLoad(String locator) {
        // TODO implement generic method to wait until page content is loaded


        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
        // ...
    }

}
